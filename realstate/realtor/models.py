from django.db import models
from datetime import datetime

# Create your models here.
class Realtor(models.Model):
	name=models.CharField(max_length=225)
	address=models.CharField(max_length=225)
	description=models.TextField(blank=True)
	is_active=models.BooleanField(default=False)
	photo=models.ImageField(upload_to='%y/%m/%d',blank=True)
	phone=models.CharField(max_length=10)
	reg_date=models.DateTimeField(default=datetime.now)
	def __str__(self):
		return self.name
