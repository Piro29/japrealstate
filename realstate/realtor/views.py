from django.shortcuts import render
from .models import Realtor
def listRealtors(request):
	realtors=Realtor.objects.all()
	context={
		'realtors':realtors
	}
	return render(request,'realtor/realtors.html',context)
def viewRealtor(request,realtorid):
	realtor=Realtor.objects.get(id=realtorId)
	context={
		'realtor':realtor
	}
	return render(request,'realtors/realtor.html',context)
