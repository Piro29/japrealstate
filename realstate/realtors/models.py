from django.db import models
from datetime import datetime

# Create your models here.

class Realtor(models.Model):
	name = models.CharField(max_length=255)
	phone = models.CharField(max_length=255)
	address = models.CharField(max_length=255)
	description = models.TextField(blank = True)
	gender = models.CharField(max_length=255)
	affiliations = models.CharField(max_length=255)
	is_active = models.BooleanField(default=True)
	reg_date=models.DateTimeField(default = datetime.now)
	def _str_(self):
		return self.name